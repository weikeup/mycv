# MYCV (C++/CLI)
An useful class for editing jpeg image.

## How to use
Just include and use it.
```cpp
#include "MYCV.h"
using namespace MYCV;
```
Example: 
```cpp
// Create an object from a jpeg image file.
JBmap^ jbmap = gcnew JBmap("sample.jpg");

// To edit image, call JBmap::LockBit() first, and call JBmap::UnlockBit() after editing.
jbmap->LockBit()
    ->ColorToGray()
    ->UnlockBit();

// Use JBmap::ToBitmap() to convert it to Bitmap.
pictureBox1->Image = jbmap->ToBitmap();
```
