#pragma once

using namespace System;
using namespace System::Drawing;
using namespace System::Drawing::Imaging;
using namespace System::Collections::Generic;
namespace MYCV
{
	public enum class ZoomMode
	{
		FIRST_ORDER,
		BILINEAR_INTERPOLATION
	};
	public ref class JPEGFormatException :Exception
	{
	public: JPEGFormatException() :Exception("The format of the image is not JPEG.") {}
	};

	public ref class JBmap
	{
	public: ref class JBmapEdit
	{
	private: JBmap^ jbMap = nullptr;
	private: Bitmap^ self = nullptr;
	private: BitmapData^ selfData = nullptr;
	private: int selfByteSkip = 0;

	public: property int ByteSkip {
		int get()
		{
			return selfByteSkip;
		}
	}

	public: JBmapEdit(JBmap^ jbMap, Bitmap^ bitmap, Rectangle rect, ImageLockMode lockMode)
	{
		this->jbMap = jbMap;
		self = bitmap;
		selfData = self->LockBits(
			rect,
			lockMode,
			self->PixelFormat
		);
		selfByteSkip = selfData->Stride - selfData->Width * 3;
	}

	public: JBmap^ UnlockBit()
	{
		if (selfData != nullptr)
		{
			self->UnlockBits(selfData);
			selfData = nullptr;
		}
		jbMap->SetBitmap(self);
		return jbMap;
	}

	public: Byte* GetPixel(int x, int y)
	{
		if (x < 0 || y < 0 || x >= self->Width || y >= self->Height)
		{
			throw gcnew ArgumentOutOfRangeException();
		}

		Byte* ptr = ((Byte*)(void*)selfData->Scan0) + x * 3 + selfData->Stride * y;

		return ptr;
	}

	public: void SetPixel(int x, int y, Byte r, Byte g, Byte b)
	{
		if (x < 0 || y < 0 || x >= self->Width || y >= self->Height)
		{
			throw gcnew ArgumentOutOfRangeException();
		}

		Byte* ptr = ((Byte*)(void*)selfData->Scan0) + x * 3 + selfData->Stride * y;

		ptr[0] = b;
		ptr[1] = g;
		ptr[2] = r;
	}

	public: JBmapEdit^ ColorToGray()
	{
		auto selfPtr = (Byte*)(void*)selfData->Scan0;

		for (auto y = 0; y < selfData->Height; y++)
		{
			for (auto x = 0; x < selfData->Width; x++)
			{
				Byte pixel = 0.114 * selfPtr[0] + 0.587 * selfPtr[1] + 0.299 * selfPtr[2];
				selfPtr[0] = pixel;
				selfPtr[1] = pixel;
				selfPtr[2] = pixel;

				selfPtr += 3;
			}

			selfPtr += selfByteSkip;
		}

		return this;
	}

	public: JBmapEdit^ CombineImage(Bitmap^ img, float ratio)
	{
		if (img->Width != self->Width && img->Height != self->Height)
		{
			return this;
		}

		auto imgData = img->LockBits(
			Rectangle(0, 0, img->Width, img->Height),
			Imaging::ImageLockMode::ReadOnly,
			img->PixelFormat
		);

		auto selfPtr = (Byte*)(void*)selfData->Scan0;
		auto imgPtr = (Byte*)(void*)imgData->Scan0;

		auto imgByteSkip = imgData->Stride - imgData->Width * 3;

		for (auto y = 0; y < selfData->Height; y++)
		{
			for (auto x = 0; x < selfData->Width; x++)
			{
				Byte pixel = selfPtr[0] * ratio + imgPtr[0] * (1 - ratio);
				selfPtr[0] = pixel;
				selfPtr[1] = pixel;
				selfPtr[2] = pixel;

				selfPtr += 3;
				imgPtr += 3;
			}

			selfPtr += selfByteSkip;
			imgPtr += imgByteSkip;
		}

		img->UnlockBits(imgData);
		return this;
	}

#pragma region Histogram Equalization

	private: int* calCDF(BitmapData^ bitmapData)
	{
		auto bitmapPtr = (Byte*)(void*)bitmapData->Scan0;
		int bitmapSkipByte = bitmapData->Stride - bitmapData->Width * 3;
		int numOfPixel = bitmapData->Width * bitmapData->Height;
		int pixelCount[256] = { 0 };
		int cdf[256] = { 0 };

		for (auto y = 0; y < bitmapData->Height; y++)
		{
			for (auto x = 0; x < bitmapData->Width; x++)
			{
				++pixelCount[bitmapPtr[0]];
				bitmapPtr += 3;
			}
			bitmapPtr += bitmapSkipByte;
		}

		for (auto i = 1; i < 256; i++)
		{
			cdf[i] += cdf[i - 1] + pixelCount[i];
		}

		return cdf;
	}

	public: JBmapEdit^ GlobalHistogramEqualization()
	{
		auto selfPtr = (Byte*)(void*)selfData->Scan0;
		auto selfByteSkip = selfData->Stride - selfData->Width * 3;

		int numOfPixel = selfData->Width * selfData->Height;
		int* cdf = calCDF(selfData);

		int cdfMin = Int32::MaxValue;
		for (auto i = 0; i < 256; i++)
		{
			if (cdf[i] < cdfMin)
			{
				cdfMin = cdf[i];
			}
		}

		selfPtr = (Byte*)(void*)selfData->Scan0;
		for (auto y = 0; y < selfData->Height; y++)
		{
			for (auto x = 0; x < selfData->Width; x++)
			{
				auto value = (Byte)Math::Round(
					(cdf[selfPtr[0]] - cdfMin) * 255.0 / (numOfPixel - cdfMin)
				);
				selfPtr[0] = value;
				selfPtr[1] = value;
				selfPtr[2] = value;
				selfPtr += 3;
			}
			selfPtr += selfByteSkip;
		}

		return this;
	}

	public: JBmapEdit^ LocalHistogramEqualization(int n)
	{
		auto result = gcnew Bitmap(self->Width, self->Height, self->PixelFormat);
		auto resultData = result->LockBits(
			Rectangle(0, 0, result->Width, result->Height),
			ImageLockMode::ReadWrite,
			result->PixelFormat
		);

		auto selfPtr = (Byte*)(void*)selfData->Scan0;
		auto resultPtr = (Byte*)(void*)resultData->Scan0;
		auto resultSkipByte = resultData->Stride - resultData->Width * 3;

		int limitN = n / 2 + 1;

		for (auto y = 0; y < selfData->Height; y++)
		{
			for (auto x = 0; x < selfData->Width; x++)
			{
				if (x >= limitN && x < selfData->Width - limitN && y >= limitN && y < selfData->Height - limitN)
				{
					auto cropedBitmap = self->Clone(
						Rectangle(x - limitN, y - limitN, n, n),
						self->PixelFormat
					);
					auto cropedBitmapData = cropedBitmap->LockBits(
						Rectangle(0, 0, cropedBitmap->Width, cropedBitmap->Height),
						ImageLockMode::ReadOnly,
						cropedBitmap->PixelFormat
					);

					int* cdf = calCDF(cropedBitmapData);
					int cdfMin = Int32::MaxValue;

					for (int i = 0; i < 256; i++)
					{
						if (cdf[i] < cdfMin)
						{
							cdfMin = cdf[i];
						}
					}

					auto resultPixel = ((Byte*)(void*)cropedBitmapData->Scan0) + cropedBitmapData->Stride * limitN + limitN;
					auto value = (Byte)Math::Round(
						(cdf[resultPixel[0]] - cdfMin) * 255.0 / (n * n - cdfMin)
					);

					resultPtr[0] = value;
					resultPtr[1] = value;
					resultPtr[2] = value;

					cropedBitmap->UnlockBits(cropedBitmapData);
				}

				selfPtr += 3;
				resultPtr += 3;
			}

			selfPtr += selfByteSkip;
			resultPtr += resultSkipByte;
		}

		self->UnlockBits(selfData);

		self = result;
		selfData = resultData;
		selfByteSkip = resultSkipByte;
		return this;
	}

#pragma endregion

#pragma region Zoom Image

	public: JBmapEdit^ Zoom(ZoomMode mode, float ratio)
	{
		if (ratio == 1)
		{
			return this;
		}

		switch (mode)
		{
		case ZoomMode::FIRST_ORDER:
		{
			firstOrderZoom(ratio);
			break;
		}
		case ZoomMode::BILINEAR_INTERPOLATION:
		{
			bilinearZoom(ratio);
			break;
		}
		}

		return this;
	}

	private: void firstOrderZoom(float ratio)
	{
		Bitmap^ result = nullptr;
		result = gcnew Bitmap(self->Width, self->Height, self->PixelFormat);

		auto resultData = result->LockBits(
			Rectangle(0, 0, result->Width, result->Height),
			ImageLockMode::ReadWrite,
			result->PixelFormat
		);

		auto selfPtr = (Byte*)(void*)selfData->Scan0;
		auto resultPtr = (Byte*)(void*)resultData->Scan0;

		auto resultByteSkip = resultData->Stride - resultData->Width * 3;

		int selfX = 0;
		int selfY = 0;
		float r = 0;

		for (int y = 0; y < selfData->Height; y++)
		{
			for (int x = 0; x < selfData->Width; x++)
			{
				selfX = x / ratio;
				selfY = y / ratio;
				r = (float)Decimal::Remainder(x, Decimal(ratio));
				if (selfY >= 0 && selfX >= 0 && selfY < selfData->Height && selfX < selfData->Width - 1)
				{
					resultPtr[0] = (Byte)((double)((*(selfPtr + (selfX * 3) + (selfY * selfData->Stride))) * (ratio - r) + ((*(selfPtr + (selfX * 3) + (selfY * selfData->Stride) + 3)) * (r))) / ratio);
					resultPtr[1] = (Byte)((double)((*(selfPtr + (selfX * 3) + (selfY * selfData->Stride) + 1)) * (ratio - r) + ((*(selfPtr + (selfX * 3) + (selfY * selfData->Stride) + 3 + 1)) * (r))) / ratio);
					resultPtr[2] = (Byte)((double)((*(selfPtr + (selfX * 3) + (selfY * selfData->Stride) + 2)) * (ratio - r) + ((*(selfPtr + (selfX * 3) + (selfY * selfData->Stride) + 3 + 2)) * (r))) / ratio);
				}
				else
				{
					resultPtr[0] = resultPtr[1] = resultPtr[2] = (Byte)255;
				}
				resultPtr += 3;
			}
			resultPtr += resultByteSkip;
		}

		resultPtr = (Byte*)(void*)resultData->Scan0;

		for (int y = 0; y < selfData->Height; y++)
		{
			for (int x = 0; x < selfData->Width; x++)
			{
				selfX = x / ratio;
				selfY = y / ratio;
				r = (float)Decimal::Remainder(y, Decimal(ratio));
				if (selfY >= 0 && selfX >= 0 && selfY < selfData->Height && selfX < selfData->Width - 1)
				{
					selfPtr = (Byte*)(void*)selfData->Scan0;
					resultPtr[0] = (Byte)(int)((double)((*(selfPtr + (selfX * 3) + (selfY * selfData->Stride))) * (ratio - r) + ((*(selfPtr + (selfX * 3) + (selfY * selfData->Stride) + 3)) * (r))) / ratio);
					resultPtr[1] = (Byte)(int)((double)((*(selfPtr + (selfX * 3) + (selfY * selfData->Stride) + 1)) * (ratio - r) + ((*(selfPtr + (selfX * 3) + (selfY * selfData->Stride) + 3 + 1)) * (r))) / ratio);
					resultPtr[2] = (Byte)(int)((double)((*(selfPtr + (selfX * 3) + (selfY * selfData->Stride) + 2)) * (ratio - r) + ((*(selfPtr + (selfX * 3) + (selfY * selfData->Stride) + 3 + 2)) * (r))) / ratio);
				}
				else
				{
					resultPtr[0] = 255;
					resultPtr[1] = 255;
					resultPtr[2] = 255;
				}
				resultPtr += 3;
			}
			resultPtr += resultByteSkip;
		}

		self->UnlockBits(selfData);

		self = result;
		selfData = resultData;
		selfByteSkip = resultByteSkip;
	}

	private: void bilinearZoom(float ratio)
	{
		Bitmap^ result = nullptr;
		result = gcnew Bitmap(self->Width, self->Height, self->PixelFormat);

		auto resultData = result->LockBits(
			Rectangle(0, 0, result->Width, result->Height),
			ImageLockMode::ReadWrite,
			result->PixelFormat
		);

		auto selfPtr = (Byte*)(void*)selfData->Scan0;
		auto resultPtr = (Byte*)(void*)resultData->Scan0;

		auto resultByteSkip = resultData->Stride - resultData->Width * 3;

		float tempR = 0;
		float tempG = 0;
		float tempB = 0;
		float selfY = 0;
		float selfX = 0;
		int x1 = 0;
		int y1 = 0;

		for (int y = 1; y < selfData->Height - 1; y++)
		{
			for (int x = 1; x < selfData->Width - 1; x++)
			{
				int values[4][3];

				selfX = x / ratio;
				selfY = y / ratio;
				x1 = selfX;
				y1 = selfY;

				for (int xx = 0; xx <= 1; xx++)
				{
					for (int yy = 0; yy <= 1; yy++)
					{
						selfPtr = (Byte*)(void*)selfData->Scan0;
						selfPtr += (y1 + yy) * selfData->Stride + (x1 + xx) * 3;
						if (y1 + yy >= selfData->Height || x1 + xx >= selfData->Width)
						{
							values[yy * 1 + xx * 2][0] = 255;
							values[yy * 1 + xx * 2][1] = 255;
							values[yy * 1 + xx * 2][2] = 255;
						}
						else
						{
							values[yy * 1 + xx * 2][0] = selfPtr[0];
							values[yy * 1 + xx * 2][1] = selfPtr[1];
							values[yy * 1 + xx * 2][2] = selfPtr[2];
						}
					}
				}
				tempR = values[0][2] * (x1 + 1 - selfX) * (y1 + 1 - selfY) +
					values[1][2] * (x1 + 1 - selfX) * (selfY - y1) +
					values[2][2] * (selfX - x1) * (y1 + 1 - selfY) +
					values[3][2] * (selfX - x1) * (selfY - y1);
				tempG = values[0][1] * (x1 + 1 - selfX) * (y1 + 1 - selfY) +
					values[1][1] * (x1 + 1 - selfX) * (selfY - y1) +
					values[2][1] * (selfX - x1) * (y1 + 1 - selfY) +
					values[3][1] * (selfX - x1) * (selfY - y1);
				tempB = values[0][0] * (x1 + 1 - selfX) * (y1 + 1 - selfY) +
					values[1][0] * (x1 + 1 - selfX) * (selfY - y1) +
					values[2][0] * (selfX - x1) * (y1 + 1 - selfY) +
					values[3][0] * (selfX - x1) * (selfY - y1);

				resultPtr = (Byte*)(void*)resultData->Scan0;
				resultPtr += y * resultData->Stride + x * 3;

				resultPtr[0] = (Byte)tempB;
				resultPtr[1] = (Byte)tempG;
				resultPtr[2] = (Byte)tempR;

				resultPtr += 3;
			}
		}

		self->UnlockBits(selfData);

		self = result;
		selfData = resultData;
		selfByteSkip = resultByteSkip;
	}

#pragma endregion

#pragma region Noise
	public:  JBmapEdit^ AddNoiseByGaussian(int mean, int stdDeviation)
	{
		Random^ random = gcnew Random(DateTime::Now.Millisecond);

		auto selfPtr = (Byte*)(void*)selfData->Scan0;

		for (int y = 0; y < selfData->Height; y++)
		{
			for (int x = 0; x < selfData->Width; x++)
			{
				float u = random->NextDouble();
				float v = random->NextDouble();
				float pixel = Math::Sqrt(-2 * Math::Log(u)) * Math::Cos(2 * Math::PI * v) * stdDeviation + mean;

				if (pixel > 127)
				{
					pixel = 127;
				}
				else if (pixel < -128)
				{
					pixel = -128;
				}

				for (int i = 0; i < 3; i++)
				{
					if (selfPtr[i] + pixel > 255)
					{
						selfPtr[i] = 255;
					}
					else if (selfPtr[i] + pixel < 0)
					{
						selfPtr[i] = 0;
					}
					else
					{
						selfPtr[i] += pixel;
					}
				}

				selfPtr += 3;
			}
		}

		return this;
	}

		  /// <summary>
		  /// Image corrupted by impulse noise.
		  /// </summary>
		  /// <exception cref="System.ArgumentException">
		  /// The maximum of ratio is 1.
		  /// </exception>
		  /// <param name="ratio">
		  /// The rate of noise.
		  /// </param>
		  /// <returns>
		  /// JBmapEdit
		  /// </returns>
	public:  JBmapEdit^ AddNoiseByImpulse(double ratio)
	{
		if (ratio > 1)
		{
			throw gcnew ArgumentException("Maximum ratio is 1!");
		}

		Bitmap^ result = self->Clone(Rectangle(0, 0, self->Width, self->Height), self->PixelFormat);
		BitmapData^ resultData = result->LockBits(Rectangle(0, 0, result->Width, result->Height), ImageLockMode::ReadWrite, result->PixelFormat);

		Byte* selfPtr = (Byte*)((void*)selfData->Scan0);
		Byte* resultPtr = (Byte*)((void*)resultData->Scan0);

		int resultByteSkip = resultData->Stride - result->Width * 3;
		int length = self->Width * self->Height;

		int pepper = length * ratio / 2;
		int salt = length * ratio / 2;

		array<bool>^ pixelCheck = gcnew array<bool>(length) { false };
		Random^ rand = gcnew Random(DateTime::Now.Millisecond);
		for (int i = 0; i < pepper; i++)
		{
			int x = rand->Next(self->Width);
			int y = rand->Next(self->Height);
			int position = x * y;
			if (pixelCheck[position] == false)
			{
				pixelCheck[position] = true;

				resultPtr = (Byte*)((void*)resultData->Scan0);
				resultPtr += y * resultData->Stride;
				resultPtr += x * 3;

				resultPtr[0] = 0;
				resultPtr[1] = 0;
				resultPtr[2] = 0;
			}
		}

		for (int i = 0; i < salt; i++)
		{
			int x = rand->Next(self->Width);
			int y = rand->Next(self->Height);
			int position = x * y;
			if (pixelCheck[position] == false)
			{
				pixelCheck[position] = true;

				resultPtr = (Byte*)((void*)resultData->Scan0);
				resultPtr += y * resultData->Stride;
				resultPtr += x * 3;

				resultPtr[0] = 255;
				resultPtr[1] = 255;
				resultPtr[2] = 255;
			}
		}

		self->UnlockBits(selfData);
		self = result;
		selfData = resultData;
		selfByteSkip = resultByteSkip;
		return this;
	}

#pragma endregion

#pragma region Filter
	public: JBmapEdit^ LowpassFilter(int size)
	{
		auto result = self->Clone(
			Rectangle(0, 0, self->Width, self->Height),
			self->PixelFormat
		);
		auto resultData = result->LockBits(
			Rectangle(0, 0, result->Width, result->Height),
			ImageLockMode::ReadWrite,
			result->PixelFormat
		);
		auto resultByteSkip = resultData->Stride - resultData->Width * 3;
		auto resultPtr = (Byte*)(void*)resultData->Scan0;
		auto selfPtr = (Byte*)(void*)selfData->Scan0;
		int skip = size / 2;
		int bSum = 0;
		int gSum = 0;
		int rSum = 0;
		int runTimeCount = 0;

		for (int y = 0; y < selfData->Height; y++)
		{
			for (int x = 0; x < selfData->Width; x++)
			{
				for (int yy = y - skip; yy <= y + skip; yy++)
				{
					for (int xx = x - skip; xx <= x + skip; xx++)
					{
						if (xx >= 0 && yy >= 0 && xx < selfData->Width && yy < selfData->Height)
						{
							++runTimeCount;
							selfPtr = (Byte*)(void*)resultData->Scan0;
							selfPtr += yy * resultData->Stride + xx * 3;
							bSum += selfPtr[0];
							gSum += selfPtr[1];
							rSum += selfPtr[2];
						}
					}
				}

				resultPtr[0] = bSum / runTimeCount;
				resultPtr[1] = gSum / runTimeCount;
				resultPtr[2] = rSum / runTimeCount;

				bSum = 0;
				gSum = 0;
				rSum = 0;
				runTimeCount = 0;
				resultPtr += 3;
			}
			resultPtr += resultByteSkip;
		}

		self->UnlockBits(selfData);

		self = result;
		selfData = resultData;
		selfByteSkip = resultByteSkip;

		return this;
	}

		  /// <summary>
		  /// Noise reduction by median filger.
		  /// </summary>
		  /// <param name="size">
		  /// The length of square mask.
		  /// </param>
		  /// <returns>
		  /// JBmapEdit
		  /// </returns>
	public: JBmapEdit^ MedianFilter(int size)
	{
		Bitmap^ result = self->Clone(
			Rectangle(0, 0, self->Width, self->Height),
			self->PixelFormat
		);
		BitmapData^ resultData = result->LockBits(
			Rectangle(0, 0, result->Width, result->Height),
			ImageLockMode::ReadWrite, result->PixelFormat
		);
		Byte* selfPtr = (Byte*)(Void*)selfData->Scan0;
		Byte* resultPtr = (Byte*)(Void*)resultData->Scan0;
		int resultByteSkip = selfData->Stride - self->Width * 3;

		List<int>^ bluePixelSet = gcnew List<int>();
		List<int>^ greenPixelSet = gcnew List<int>();
		List<int>^ redPixelSet = gcnew List<int>();
		int skip = size / 2;
		int timesCount = 0;
		for (int cursorY = 0; cursorY < self->Height; cursorY++)
		{
			for (int cursorX = 0; cursorX < self->Width; cursorX++)
			{
				for (int maskY = cursorY - skip; maskY <= cursorY + skip; maskY++)
				{
					for (int maskX = cursorX - skip; maskX <= cursorX + skip; maskX++)
					{
						if (maskX >= 0 && maskY >= 0 && maskX < self->Width && maskY < self->Height)
						{
							timesCount++;
							selfPtr = (Byte*)(Void*)selfData->Scan0;
							selfPtr += maskY * selfData->Stride + maskX * 3;

							bluePixelSet->Add(selfPtr[0]);
							greenPixelSet->Add(selfPtr[1]);
							redPixelSet->Add(selfPtr[2]);
						}
					}
				}
				bluePixelSet->Sort();
				greenPixelSet->Sort();
				redPixelSet->Sort();
				int position = timesCount / 2;
				resultPtr[0] = bluePixelSet[position];
				resultPtr[1] = greenPixelSet[position];
				resultPtr[2] = redPixelSet[position];

				bluePixelSet->Clear();
				greenPixelSet->Clear();
				redPixelSet->Clear();
				timesCount = 0;
				resultPtr += 3;
			}
			resultPtr += resultByteSkip;
		}

		self->UnlockBits(selfData);
		self = result;
		selfData = resultData;
		selfByteSkip = resultByteSkip;
		return this;
	}

	public: JBmapEdit^ AlphaTrimmedFilter(int size, int offset)
	{
		auto result = gcnew Bitmap(self->Width, self->Height, self->PixelFormat);
		auto resultData = result->LockBits(
			Rectangle(0, 0, result->Width, result->Height),
			ImageLockMode::ReadWrite,
			result->PixelFormat
		);

		auto selfPtr = (Byte*)(void*)selfData->Scan0;
		auto resultPtr = (Byte*)(void*)resultData->Scan0;

		int resultByteSkip = resultData->Stride - resultData->Width * 3;

		int blank = size / 2;

		for (int y = 0; y < selfData->Height; y++)
		{
			for (int x = 0; x < selfData->Width; x++)
			{
				if (x - blank < 0 || x + blank >= selfData->Width || y - blank < 0 || y + blank >= selfData->Height)
				{
					resultPtr[0] = selfPtr[y * selfData->Stride + x * 3];
					resultPtr[1] = selfPtr[y * selfData->Stride + x * 3 + 1];
					resultPtr[2] = selfPtr[y * selfData->Stride + x * 3 + 2];
				}
				else
				{
					int totalSize = size * size;
					List<int>^ data = gcnew List<int>(totalSize);

					for (int yy = -blank; yy <= blank; yy++)
					{
						for (int xx = -blank; xx <= blank; xx++)
						{
							data->Add(selfPtr[(y + yy) * selfData->Stride + (x + xx) * 3]);
						}
					}
					data->Sort();
					int sum = 0;
					for (int i = offset; i < totalSize - offset; i++)
					{
						sum += data[i];
					}
					sum /= totalSize - 2 * offset;
					resultPtr[0] = (Byte)sum;
					resultPtr[1] = (Byte)sum;
					resultPtr[2] = (Byte)sum;
				}
				resultPtr += 3;
			}
			resultPtr += resultByteSkip;
		}

		self->UnlockBits(selfData);

		self = result;
		selfData = resultData;
		selfByteSkip = resultByteSkip;

		return this;
	}

	public: JBmapEdit^ SpatialHighPassFilter(int size)
	{
		Bitmap^ result = self->Clone(
			Rectangle(0, 0, self->Width, self->Height),
			self->PixelFormat
		);
		BitmapData^ resultData = result->LockBits(
			Rectangle(0, 0, result->Width, result->Height),
			ImageLockMode::ReadWrite,
			result->PixelFormat
		);
		Byte* selfPtr = (Byte*)(Void*)selfData->Scan0;
		Byte* resultPtr = (Byte*)(Void*)resultData->Scan0;
		int resultByteSkip = selfData->Stride - self->Width * 3;

		int bRunningSum = 0;
		int gRunningSum = 0;
		int rRunningSum = 0;
		int skip = size / 2;
		int coefficient = 0;
		int timesCount = 0;
		for (int cursorY = 0; cursorY < self->Height; cursorY++)
		{
			for (int cursorX = 0; cursorX < self->Width; cursorX++)
			{
				for (int maskY = cursorY - skip; maskY <= cursorY + skip; maskY++)
				{
					for (int maskX = cursorX - skip; maskX <= cursorX + skip; maskX++)
					{
						if (maskX >= 0 && maskY >= 0 && maskX < self->Width && maskY < self->Height)
						{
							timesCount++;
							selfPtr = (Byte*)(Void*)selfData->Scan0;
							selfPtr += maskY * selfData->Stride + maskX * 3;

							if (maskX == cursorX && maskY == cursorY)
								coefficient = size * size - 1;
							else
								coefficient = -1;

							bRunningSum += selfPtr[0] * coefficient;
							gRunningSum += selfPtr[1] * coefficient;
							rRunningSum += selfPtr[2] * coefficient;
						}
					}
				}

				if (bRunningSum < 0) bRunningSum = 0;
				int avg = bRunningSum / timesCount;
				resultPtr[0] = avg;

				if (gRunningSum < 0) gRunningSum = 0;
				avg = gRunningSum / timesCount;
				resultPtr[1] = avg;

				if (rRunningSum < 0) rRunningSum = 0;
				avg = rRunningSum / timesCount;
				resultPtr[2] = avg;

				bRunningSum = 0;
				gRunningSum = 0;
				rRunningSum = 0;
				timesCount = 0;
				resultPtr += 3;
			}
			resultPtr += resultByteSkip;
		}

		self->UnlockBits(selfData);
		self = result;
		selfData = resultData;
		selfByteSkip = resultByteSkip;
		return this;
	}

	public: JBmapEdit^ SpatialHighBoostFilter(int size, double A)
	{
		Bitmap^ result = self->Clone(
			Rectangle(0, 0, self->Width, self->Height),
			self->PixelFormat
		);
		BitmapData^ resultData = result->LockBits(
			Rectangle(0, 0, result->Width, result->Height),
			ImageLockMode::ReadWrite,
			result->PixelFormat
		);
		Byte* selfPtr = (Byte*)(Void*)selfData->Scan0;
		Byte* resultPtr = (Byte*)(Void*)resultData->Scan0;
		int resultByteSkip = selfData->Stride - self->Width * 3;

		int bRunningSum = 0;
		int gRunningSum = 0;
		int rRunningSum = 0;
		int skip = size / 2;
		int coefficient = 0;
		int timesCount = 0;
		for (int cursorY = 0; cursorY < self->Height; cursorY++)
		{
			for (int cursorX = 0; cursorX < self->Width; cursorX++)
			{
				for (int maskY = cursorY - skip; maskY <= cursorY + skip; maskY++)
				{
					for (int maskX = cursorX - skip; maskX <= cursorX + skip; maskX++)
					{
						if (maskX >= 0 && maskY >= 0 && maskX < self->Width && maskY < self->Height)
						{
							timesCount++;
							selfPtr = (Byte*)(Void*)selfData->Scan0;
							selfPtr += maskY * selfData->Stride + maskX * 3;

							if (maskX == cursorX && maskY == cursorY)
								coefficient = A + (size * size - 2);
							else
								coefficient = -1;

							bRunningSum += selfPtr[0] * coefficient;
							gRunningSum += selfPtr[1] * coefficient;
							rRunningSum += selfPtr[2] * coefficient;
						}
					}
				}

				if (bRunningSum < 0) bRunningSum = 0;
				else if (bRunningSum > 255) bRunningSum = 255;
				resultPtr[0] = bRunningSum;

				if (gRunningSum < 0) gRunningSum = 0;
				else if (gRunningSum > 255) gRunningSum = 255;
				resultPtr[1] = bRunningSum;

				if (rRunningSum < 0) rRunningSum = 0;
				else if (rRunningSum > 255) rRunningSum = 255;
				resultPtr[2] = rRunningSum;

				bRunningSum = 0;
				gRunningSum = 0;
				rRunningSum = 0;
				timesCount = 0;
				resultPtr += 3;
			}
			resultPtr += resultByteSkip;
		}

		self->UnlockBits(selfData);
		self = result;
		selfData = resultData;
		selfByteSkip = resultByteSkip;
		return this;
	}

	public: JBmapEdit^ DerivativeFilter(int size, List<int>^ coefficient)
	{
		Bitmap^ result = self->Clone(
			Rectangle(0, 0, self->Width, self->Height),
			self->PixelFormat
		);
		BitmapData^ resultData = result->LockBits(
			Rectangle(0, 0, result->Width, result->Height),
			ImageLockMode::ReadWrite, result->PixelFormat
		);
		Byte* selfPtr = (Byte*)(void*)selfData->Scan0;
		Byte* resultPtr = (Byte*)(void*)resultData->Scan0;
		int resultByteSkip = resultData->Stride - resultData->Width * 3;

		List<int>^ bluePixelSet = gcnew List<int>();
		List<int>^ greenPixelSet = gcnew List<int>();
		List<int>^ redPixelSet = gcnew List<int>();
		int skip = size / 2;
		int timesCount = 0;
		for (int cursorY = 0; cursorY < selfData->Height; cursorY++)
		{
			for (int cursorX = 0; cursorX < selfData->Width; cursorX++)
			{
				for (int maskY = cursorY - skip; maskY <= cursorY + skip; maskY++)
				{
					for (int maskX = cursorX - skip; maskX <= cursorX + skip; maskX++)
					{
						if (maskX >= 0 && maskY >= 0 && maskX < selfData->Width && maskY < selfData->Height)
						{
							timesCount++;
							selfPtr = (Byte*)(Void*)selfData->Scan0;
							selfPtr += maskY * selfData->Stride + maskX * 3;

							bluePixelSet->Add(selfPtr[0]);
							greenPixelSet->Add(selfPtr[1]);
							redPixelSet->Add(selfPtr[2]);
						}
					}
				}

				if (timesCount == size * size)
				{
					int intensity = Math::Abs(bluePixelSet[0] * coefficient[0]
						+ bluePixelSet[1] * coefficient[1]
						+ bluePixelSet[2] * coefficient[2]
						+ bluePixelSet[3] * coefficient[3]
						+ bluePixelSet[4] * coefficient[4]
						+ bluePixelSet[5] * coefficient[5]
						+ bluePixelSet[6] * coefficient[6]
						+ bluePixelSet[7] * coefficient[7]
						+ bluePixelSet[8] * coefficient[8]);

					if (intensity > 255) intensity = 255;
					resultPtr[0] = intensity;
					resultPtr[1] = intensity;
					resultPtr[2] = intensity;
				}

				bluePixelSet->Clear();
				greenPixelSet->Clear();
				redPixelSet->Clear();
				timesCount = 0;
				resultPtr += 3;
			}
			resultPtr += resultByteSkip;
		}

		self->UnlockBits(selfData);
		self = result;
		selfData = resultData;
		selfByteSkip = resultByteSkip;

		return this;
	}

	public: JBmapEdit^ LaplacianDiagonal()
	{
		List<int>^ coe = gcnew List<int>();
		coe->Add(-1);
		coe->Add(-1);
		coe->Add(-1);
		coe->Add(-1);
		coe->Add(9);
		coe->Add(-1);
		coe->Add(-1);
		coe->Add(-1);
		coe->Add(-1);

		return DerivativeFilter(3, coe);
	}

	public: JBmapEdit^ LaplacianHorizotalAndVertical()
	{
		List<int>^ coe = gcnew List<int>();
		coe->Add(0);
		coe->Add(-1);
		coe->Add(0);
		coe->Add(-1);
		coe->Add(5);
		coe->Add(-1);
		coe->Add(0);
		coe->Add(-1);
		coe->Add(0);

		return DerivativeFilter(3, coe);
	}

	public: JBmapEdit^ SobelOperatorDiagonal()
	{
		List<int>^ coe = gcnew List<int>();
		coe->Add(-2);
		coe->Add(0);
		coe->Add(2);
		coe->Add(-2);
		coe->Add(0);
		coe->Add(2);
		coe->Add(-2);
		coe->Add(0);
		coe->Add(2);

		return DerivativeFilter(3, coe);
	}

	public: JBmapEdit^ SobelOperatorHorizotalAndVertical()
	{
		List<int>^ coe = gcnew List<int>();
		coe->Add(-2);
		coe->Add(-2);
		coe->Add(0);
		coe->Add(-2);
		coe->Add(0);
		coe->Add(2);
		coe->Add(0);
		coe->Add(2);
		coe->Add(2);

		return DerivativeFilter(3, coe);
	}

	public: JBmapEdit^ PrewittOperatorDiagonal()
	{
		List<int>^ coe = gcnew List<int>();
		coe->Add(-1);
		coe->Add(0);
		coe->Add(1);
		coe->Add(-2);
		coe->Add(0);
		coe->Add(2);
		coe->Add(-1);
		coe->Add(0);
		coe->Add(1);

		return DerivativeFilter(3, coe);
	}

	public: JBmapEdit^ PrewittOperatorHorizontalAndVertical()
	{
		List<int>^ coe = gcnew List<int>();
		coe->Add(-2);
		coe->Add(-1);
		coe->Add(0);
		coe->Add(-1);
		coe->Add(0);
		coe->Add(1);
		coe->Add(0);
		coe->Add(1);
		coe->Add(2);

		return DerivativeFilter(3, coe);
	}
#pragma endregion

#pragma region Compress

	private: Double calBasicImage(int n, int x, int y, int u, int v)
	{
		Double A = (2 * x + 1) * u * Math::PI / (2 * n);
		Double B = (2 * y + 1) * v * Math::PI / (2 * n);
		Double res = Math::Cos(A) * Math::Cos(B);

		return res;
	}

	private: Double calC(int cursorX, int cursorY, int u, int v, int blockSize)
	{
		Double au = u == 0 ? 0.35355339 : 0.5;
		Double av = v == 0 ? 0.35355339 : 0.5;
		Double pixelSum = 0;
		Byte* selfPtr = (Byte*)(void*)selfData->Scan0;

		for (int y = 0; y < blockSize; y++)
		{
			selfPtr = (Byte*)(void*)selfData->Scan0;
			selfPtr += selfData->Stride * (cursorY * blockSize + y) + cursorX * blockSize * 3;
			for (int x = 0; x < blockSize; x++)
			{
				Double basicImage = calBasicImage(blockSize, x, y, u, v);
				Double lessPixel = selfPtr[0] - 128;
				Double pixel = lessPixel * basicImage;
				pixelSum += pixel;
				selfPtr += 3;
			}
		}
		pixelSum *= au * av;
		if (u > 3 || v > 3)pixelSum = 0;

		return pixelSum;
	}

	private: int InverseC(int x, int y, int level, int blockSize, List<List<Double>^>^ C)
	{
		Double pixelSum = 0;
		for (int v = 0; v < 8; v++)
		{
			for (int u = 0; u < 8; u++)
			{
				Double au = u == 0 ? 0.35355339 : 0.5;
				Double av = v == 0 ? 0.35355339 : 0.5;
				Double basicImage = calBasicImage(blockSize, x, y, u, v);
				List <Double>^ tmpC = C[level];
				Double pixel = au * av * tmpC[u + v * 8] * basicImage;
				pixelSum += pixel;
			}
		}
		int putBack = pixelSum + 128;
		if (putBack < 0) putBack = 0;
		else if (putBack > 255) putBack = 255;

		return putBack;
	}

	public: JBmapEdit^ DCTCompress()
	{
		int blockSize = 8;
		int blockCount = selfData->Width / blockSize; //64
		List<List<Double>^>^ C = gcnew List<List<Double>^>();

		//DCT
		for (int cursorY = 0; cursorY < blockCount; cursorY++)
		{
			for (int cursorX = 0; cursorX < blockCount; cursorX++)
			{
				List<Double>^ tmpC = gcnew List<Double>();
				for (int v = 0; v < blockSize; v++)
				{
					for (int u = 0; u < blockSize; u++)
					{
						tmpC->Add(calC(cursorX, cursorY, u, v, blockSize));
					}
				}
				C->Add(tmpC);
			}
		}

		//IDCT
		Byte* selfPtr = (Byte*)(void*)selfData->Scan0;
		for (int cursorY = 0; cursorY < blockCount; cursorY++)
		{
			for (int cursorX = 0; cursorX < blockCount; cursorX++)
			{
				for (int y = 0; y < blockSize; y++)
				{
					selfPtr = (Byte*)(void*)selfData->Scan0;
					selfPtr += selfData->Stride * (cursorY * blockSize + y) + cursorX * blockSize * 3;
					for (int x = 0; x < blockSize; x++)
					{
						int level = cursorX + cursorY * 64;
						int putBack = InverseC(x, y, level, blockSize, C);
						selfPtr[0] = putBack;
						selfPtr[1] = putBack;
						selfPtr[2] = putBack;
						selfPtr += 3;
					}
				}
			}
		}
		return this;
	}

	public: JBmapEdit^ GWFCompress(int pass)
	{
		Bitmap^ result = self->Clone(
			Rectangle(0, 0, self->Width, self->Height),
			self->PixelFormat
		);
		BitmapData^ resultData = result->LockBits(
			Rectangle(0, 0, result->Width, result->Height),
			ImageLockMode::ReadWrite,
			result->PixelFormat
		);
		Byte* selfPtr = (Byte*)(void*)selfData->Scan0;
		Byte* resultPtr = (Byte*)(void*)resultData->Scan0;

		for (int times = 1; times <= pass; times++)
		{
			selfPtr = (Byte*)(void*)selfData->Scan0;
			resultPtr = (Byte*)(void*)resultData->Scan0;
			for (int y = 0; y < selfData->Height / (int)Math::Pow(2, times - 1); y++) //Horizontal
			{
				selfPtr = (Byte*)(void*)selfData->Scan0;
				selfPtr += selfData->Stride * y;
				resultPtr = (Byte*)(void*)resultData->Scan0;
				resultPtr += resultData->Stride * y;

				int w = selfData->Width / Math::Pow(2, times);
				for (int x = 0; x < w; x++)
				{
					int thisPixel = selfPtr[0];
					int nextPixel = (selfPtr + 3)[0];
					int sum = (thisPixel + nextPixel) / 2;
					int difference = (thisPixel - nextPixel) / 2;
					resultPtr[0] = sum;
					resultPtr[1] = sum;
					resultPtr[2] = sum;
					(resultPtr + w*3)[0] = difference;
					(resultPtr + w*3)[1] = difference;
					(resultPtr + w*3)[2] = difference;
					
					resultPtr += 3;
					selfPtr += 6;
				}
			}

			self->UnlockBits(selfData);
			self = result;
			selfData = resultData;

			result = self->Clone(
				Rectangle(0, 0, self->Width, self->Height),
				self->PixelFormat
			);
			resultData = result->LockBits(
				Rectangle(0, 0, result->Width, result->Height),
				ImageLockMode::ReadWrite,
				result->PixelFormat
			);
			selfPtr = (Byte*)(void*)selfData->Scan0;
			resultPtr = (Byte*)(void*)resultData->Scan0;
			
			for (int x = 0; x < selfData->Width / (int)Math::Pow(2, times - 1); x++) //Vertical
			{
				selfPtr = (Byte*)(void*)selfData->Scan0;
				selfPtr += x * 3;
				resultPtr = (Byte*)(void*)resultData->Scan0;
				resultPtr += x * 3;

				int h = selfData->Height / Math::Pow(2, times);
				for (int y = 0; y < h; y++)
				{
					int thisPixel = selfPtr[0];
					int nextPixel = (selfPtr + selfData->Stride)[0];
					int sum = (thisPixel + nextPixel) / 2;
					int difference = (thisPixel - nextPixel) / 2;
					resultPtr[0] = sum;
					resultPtr[1] = sum;
					resultPtr[2] = sum;
					(resultPtr + h * selfData->Stride)[0] = difference;
					(resultPtr + h * selfData->Stride)[1] = difference;
					(resultPtr + h * selfData->Stride)[2] = difference;

					resultPtr += resultData->Stride;
					selfPtr += selfData->Stride*2;
				}
			}
			self->UnlockBits(selfData);
			self = result;
			selfData = resultData;

			result = self->Clone(
				Rectangle(0, 0, self->Width, self->Height),
				self->PixelFormat
			);
			resultData = result->LockBits(
				Rectangle(0, 0, result->Width, result->Height),
				ImageLockMode::ReadWrite,
				result->PixelFormat
			);
		}
		self->UnlockBits(selfData);
		self = result;
		selfData = resultData;
		return this;
	}
#pragma endregion
	};



	private: Bitmap^ self = nullptr;

	public: property int Width {
		int get()
		{
			return self->Width;
		}
	}

	public: property int Height {
		int get()
		{
			return self->Height;
		}
	}

	public: JBmap(String^ path)
	{
		auto bitmap = gcnew Bitmap(path);
		if (bitmap->PixelFormat != PixelFormat::Format24bppRgb)
		{
			throw gcnew JPEGFormatException();
		}

		self = bitmap;
	}

	public: JBmap(Bitmap^ src)
	{
		if (src->PixelFormat != PixelFormat::Format24bppRgb)
		{
			throw gcnew JPEGFormatException();
		}

		self = src->Clone(
			Rectangle(0, 0, src->Width, src->Height),
			src->PixelFormat
		);
	}

	public: JBmap(Bitmap^ src, Rectangle rect)
	{
		if (src->PixelFormat != PixelFormat::Format24bppRgb)
		{
			throw gcnew JPEGFormatException();
		}

		self = src->Clone(
			rect,
			src->PixelFormat
		);
	}

	public: !JBmap()
	{
		if (self != nullptr)
		{
			self = nullptr;
		}
	}

	public: JBmap^ Clone()
	{
		return gcnew JBmap(self);
	}

	public: JBmap^ Clone(Rectangle rect)
	{
		return gcnew JBmap(self, rect);
	}

	public: JBmapEdit^ LockBit()
	{
		return LockBit(ImageLockMode::ReadWrite);
	}

	public: JBmapEdit^ LockBit(ImageLockMode lockMode)
	{
		return LockBit(
			Rectangle(0, 0, self->Width, self->Height),
			lockMode
		);
	}

	public: JBmapEdit^ LockBit(Rectangle rect, ImageLockMode lockMode)
	{
		return gcnew JBmapEdit(this, self, rect, lockMode);
	}

	public: Bitmap^ ToBitmap()
	{
		return self->Clone(
			Rectangle(0, 0, self->Width, self->Height),
			self->PixelFormat
		);
	}

	public: void SetBitmap(Bitmap^ bitmap)
	{
		if (bitmap->PixelFormat != PixelFormat::Format24bppRgb)
		{
			throw gcnew JPEGFormatException();
		}

		self = bitmap;
	}
	};
}